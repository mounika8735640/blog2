package com.dao;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.model.User;

@Service
public class UserDao {

    @Autowired
    private UserRepository userRepository;

    public List<User> getUsers() {
        return userRepository.findAll();
    }

    public User getUserById(int userId) {
        return userRepository.findById(userId).orElse(null);
    }

    public User getUserByName(String userName) {
        return userRepository.findByName(userName);
    }

    public User addUser(User user) {
        try {
            BCryptPasswordEncoder bcrypt = new BCryptPasswordEncoder();
            String encryptedPwd = bcrypt.encode(user.getPassword());
            user.setPassword(encryptedPwd);

            User savedUser = userRepository.save(user);

            // Log user information for debugging
            System.out.println("User added successfully: " + savedUser);

            return savedUser;
        } catch (DataIntegrityViolationException e) {
            // Log the exception for debugging purposes
            System.err.println("Error adding user: " + e.getMessage());
            e.printStackTrace();
            return null; // or throw a custom exception if needed
        }
    }




    public User updateUser(User user) {
        return userRepository.save(user);
    }

    public void deleteUserById(int userId) {
        userRepository.deleteById(userId);
    }

    public User userLogin(String email, String password) {
        BCryptPasswordEncoder bcrypt = new BCryptPasswordEncoder();
        User dbUser = userRepository.findByEmail(email);

        if (dbUser != null && bcrypt.matches(password, dbUser.getPassword())) {
            // Passwords match, return the user
            return dbUser;
        } else {
            // Either user not found or passwords don't match
            return null;
        }
    }
}
